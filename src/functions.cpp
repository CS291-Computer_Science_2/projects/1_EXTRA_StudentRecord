///////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 Fall 2016
//	Project: Student Record
//  File Purpose: Parse data from text files
//
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>
#include <fstream>	//ifstream
#include <sstream>
#include "headers.h"

using namespace std;
student::student(int id, string fn, string sn) {
	this->id= id;
	this->fn= fn;
	this->sn= sn;
	vector<vector<string> > holder;
};

void student::GoToClass (string filename) {
	vector<string> strV;
	vector<string> xx;
	string linex;

	ifstream f(filename);
	string line;
	vector<vector<string> > data;

	int row = 0;

	while (getline(f, line), row++){
		data.push_back(vector<string>());

		//error catch idea from Stewbond http://www.cplusplus.com/forum/unices/112048/#msg611953
		if (!f.good()) break;

		stringstream iss(line);
		for (int col = 0; col <3; col++) {
			string sv;
			getline(iss, sv, ',');		
			data[row].push_back(sv);
		}
		row++;
	}
	holder = data;
}

vector<vector<string> > student::rholder() {
	return holder;
}
