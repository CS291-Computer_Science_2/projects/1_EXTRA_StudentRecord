///////////////////////////////////////////////////////////////////
//	Name: Jose Madureira - CS 291 - Fall 2016
//  File Purpose: Creats a class holding function prototypes
//
///////////////////////////////////////////////////////////////////
#ifndef HEADERS_H
#define HEADERS_H
using namespace std;

class student {
	public:
		student();
		student (int, string, string);
		void GoToClass(string);
		vector<vector<string> > rholder();
	private:
		vector<vector<string> > holder;
		int id;
		string fn;
		string sn;
};
#endif
