////////////////////////////////////////////////////////////////////////////////////////////
//  Name: Jose Madureira	CS291-Fall 2016
//	Date:
//	Project: Create Xclass for users to input and retrieve data
//	Pseudo Code: (1) Create structure for "Xclass"
//				 (2) functions.cpp defines functions available to an object
//				 (3) main.cpp used to create an object and make it do something
////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <random>
#include "headers.h"
using namespace std;

int s2i(string s) {
	return atoi(s.c_str());
}
string i2s(int i) {
	return to_string(i);
}

vector<vector<string> > cvX(int size);

int main() {
	string rpt;
	string file="Roster.csv";
	const char cY[]="yY1";	//match pattern for rerun
	do {
		cout <<"\t..........................................\n"
		     <<"\t\t###Student Record Program###\n"
		     <<"\t     Jose Madureira - CS291 - 2016-09-24\n"
		     <<"\t..........................................\n";

		int id=0;
		string fn="",sn="";
		student *s = new student(id,fn,sn);
//	   #Pass File to Reader#
		s->GoToClass(file);

//	   #Fetch Student Data#
		vector<vector<string> > vst;
		vst = s->rholder();
		int	vsize=vst.size();
//	   #Fetch Student Classes#
		vector<vector<string> > jcl;
		jcl = cvX(vsize);

//	   #Join Data#
		vector<string> jn;
		vector<string> jc;
		string cnm,ccl;
		for(int c=1; c<vsize; c++) {
			string xid,xfn,xsn;
			xid=vst[c][0];
			xfn=vst[c][1];
			xsn=vst[c][2];
			cnm=" ("+xid+")  "+xfn+", "+xsn;
			jn.push_back(cnm);

			string c1,c2,c3,c4;
			int cm1=(c-1);	//cm1 because c doesnt start at 0
			c1=jcl[cm1][0];
			c2=jcl[cm1][1];
			c3=jcl[cm1][2];
			c4=jcl[cm1][3];
			ccl=(c1+", "+c2+", "+c3+", "+c4);
			jc.push_back(ccl);
		}
		cout<<"\n ID\tName\n";
		for(int a=0; a<jn.size(); a++) {
			cout<<jn[a]<<endl;
		}

		string xs="",xsx="x";
		int xn=0;
		cout<<"\n\t\t#Student Class Viewer#"
		    <<"\n\tEnter a Student ID Number Between '1' and "<<(vsize-1)<<": ";
		while(!(xs[0]==xsx[0])) {
			getline(cin, xs);
			if(xs=="x")break;
			xn=s2i(xs);
//		   #Catch out of Bounds
			while(xn<1||xn>vsize-1) {
				cout<<"  /!\\ "<<xs<<" is not between 1 and "<<vsize-1<<", please try again: ";
				getline(cin, xs);
				xn=s2i(xs);
			}
			cout<<"\t\t .:  Student: "<<jn[xn-1]<<"  :."
			    <<"\n\t    Classes: "<<jc[xn-1];
			cout<<"\n\n\t\tEnter Another Student(x to quit): ";
		}


		cout<<"\n\tRERUN PROGRAM?  ";
		getline(cin, rpt);
	} while(rpt[0]==cY[0] || rpt[0]==cY[1] || rpt[0]==cY[2]);	//check string start for chars(y,Y or 1)

	cout<<"\n\t";
	system("pause");
	return 0;
}
vector<vector<string> > cvX(int size) {
	const string name[9]= {"CS","MATH","HIST","SOC","ENG","PHIL","BIO","CHEM","SPCH"};
	unsigned seed = static_cast<int> (chrono::system_clock::now().time_since_epoch().count());
	mt19937 nrSeed(seed);
	uniform_int_distribution<int> nrRng(100, 500);	//class numbers
	srand(time(0));	//class names

	vector<vector<string> > strungout;
	for (int k=0; k<size; k++) {
		vector<string> temp;
		for (int i = 0; i<4; i++) {
			string combo;
			string nmT;
			int nm, nr;
//	   	   #CLASS NAMES
			nm =rand()%9;
			nmT = name[nm];
//		   #CLASS NUMBERS
			nr = nrRng(nrSeed);
			combo=(nmT+" "+i2s(nr));
			temp.push_back(combo);
		}
		strungout.push_back(temp);
	}
	return strungout;
}
